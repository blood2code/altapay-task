import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

public class Main {
    public static void main(String[] args) throws InterruptedException {
        int poolSize = 10;
        int iCount = 100_000;
        AtomicInteger counter1 = new AtomicInteger();
        AtomicInteger counter2 = new AtomicInteger();

        ExecutorService executorService = Executors.newFixedThreadPool(poolSize);

        for (int i = 0; i < iCount; i++) {
            executorService.execute(()-> {
                counter1.getAndIncrement();
                counter2.getAndIncrement();
            });
        }

        executorService.shutdown();
        executorService.awaitTermination(Long.MAX_VALUE, TimeUnit.NANOSECONDS);
        System.out.println("Counter1: " + counter1.get());
        System.out.println("Counter2: " + counter2.get());
    }
}